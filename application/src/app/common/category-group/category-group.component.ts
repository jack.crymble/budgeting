import { Component, Input, ChangeDetectorRef, ChangeDetectionStrategy, Output, EventEmitter, OnInit } from '@angular/core';
import { CategoryGroupInterface } from 'src/app/models/category-group/category-group.model';
import { MatDialog } from '@angular/material';
import { StringInputDialogComponent } from '../add-category-group/add-category-group.component';
import { CategoryGroupService } from 'src/app/services/category-group.service';
import { CategoryInterface } from 'src/app/models/category/category.model';

@Component({
    selector: 'app-category-group',
    templateUrl: './category-group.component.html',
    styleUrls: ['./category-group.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryGroupComponent implements OnInit {

    @Input() categoryGroup: CategoryGroupInterface;
    @Input() editMode = false;

    constructor(
        private dialog: MatDialog,
        private categoryGroupService: CategoryGroupService,
        private cd: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.categoryGroup.checked = false;
    }

    onCheckClick() {
        this.categoryGroup.checked = !this.categoryGroup.checked;
        this.cd.markForCheck();
    }

    addCategory() {
        const dialogRef = this.dialog.open(StringInputDialogComponent, {
            data: 'New Category Name'
        });

        dialogRef.afterClosed().subscribe(name => {
            if (name) {
                this.categoryGroup.categories.push({
                    id: Math.random() * 100,
                    name,
                    amount_budgetted: 0,
                    amount_available: 0
                } as CategoryInterface);
                this.categoryGroupService.updateCategoryGroup(this.categoryGroup).subscribe(() => {
                    this.cd.markForCheck();
                });
            }
        });
    }

}
