import { Component, OnInit, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ButtonOptionInterface } from 'src/app/models/button-option/button-option.model';
import { CategoryGroupService } from 'src/app/services/category-group.service';
import { CategoryGroupInterface } from 'src/app/models/category-group/category-group.model';
import { Subscription } from 'rxjs';
import { testCategoryGroupData } from 'src/app/models/category-group/category-group.data';
import { MatDialog } from '@angular/material';
import { StringInputDialogComponent } from '../add-category-group/add-category-group.component';

@Component({
    selector: 'app-edit-categories',
    templateUrl: './edit-categories.component.html',
    styleUrls: ['./edit-categories.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCategoriesComponent implements OnInit, OnDestroy {

    @Output() closeView: EventEmitter<string> = new EventEmitter<string>();

    headerOptions: Array<ButtonOptionInterface> = [
        {
            name: 'Close',
            icon: 'close'
        }
    ];

    footerOptions: Array<ButtonOptionInterface> = [
        {
            name: 'Delete',
            icon: 'delete'
        },
        {
            name: 'New Group',
            icon: 'add_circle_outline'
        },
    ];

    categoryGroups: Array<CategoryGroupInterface> = [];
    categoryGroupSubscription: Subscription;

    constructor(
        private categoryGroupService: CategoryGroupService,
        private cd: ChangeDetectorRef,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.updateCategoryGroups();
    }

    ngOnDestroy() {
        this.categoryGroupSubscription.unsubscribe();
    }

    onHeaderButtonClick(item: ButtonOptionInterface) {
        this.closeView.emit();
    }

    onFooterButtonClick(item: ButtonOptionInterface) {
        if (item.name === 'New Group') {
            const dialogRef = this.dialog.open(StringInputDialogComponent, {
                data: 'New Group Name'
            });

            dialogRef.afterClosed().subscribe(name => {
                if (name) {
                    const newGroup = {
                        id: Math.random() * 1000,
                        name,
                        categories: []
                    } as CategoryGroupInterface;
                    this.categoryGroupService.addCategoryGroup(newGroup).subscribe(() => {
                        this.updateCategoryGroups();
                    });
                }
            });
        } else if (item.name === 'Delete') {
            this.categoryGroups.forEach(cg => {
                if (cg.checked) {
                    this.categoryGroupService.removeCategoryGroup(cg).subscribe();
                } else {
                    cg.categories.forEach(c => {
                        if (c.checked) {
                            cg.categories.splice(cg.categories.indexOf(c), 1);
                        }
                    });
                    this.categoryGroupService.updateCategoryGroup(cg).subscribe();
                }
            });
            this.updateCategoryGroups();
        }
        this.cd.markForCheck();
    }

    updateCategoryGroups() {
        this.categoryGroupSubscription = this.categoryGroupService.getAllCategoryGroups().subscribe(resp => {
            this.categoryGroups = resp;
            this.cd.markForCheck();
        });
    }

}
