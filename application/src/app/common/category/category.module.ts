import { NgModule } from '@angular/core';

import { CategoryComponent } from './category.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatButtonModule } from '@angular/material';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        MatIconModule,
        MatButtonModule
    ],
    exports: [
        CategoryComponent,
    ],
    declarations: [
        CategoryComponent
    ],
    providers: [],
})
export class CategoryModule { }
