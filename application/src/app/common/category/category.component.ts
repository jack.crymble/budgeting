import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { CategoryInterface } from 'src/app/models/category/category.model';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryComponent implements OnInit {

    @Input() category: CategoryInterface;
    @Input() editMode = false;

    constructor(
        private cd: ChangeDetectorRef
    ) {}

    ngOnInit() {
        this.category.checked = false;
    }

    onCheckClick() {
        this.category.checked = !this.category.checked;
        this.cd.markForCheck();
    }

}
