import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StringInputDialogComponent } from './add-category-group.component';

describe('AddCategoryGroupComponent', () => {
  let component: StringInputDialogComponent;
  let fixture: ComponentFixture<StringInputDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StringInputDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StringInputDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
