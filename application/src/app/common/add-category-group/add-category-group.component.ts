import { Component, OnInit, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-add-category-group',
    templateUrl: './add-category-group.component.html',
    styleUrls: ['./add-category-group.component.scss']
})
export class StringInputDialogComponent implements OnInit {

    name: FormControl;

    constructor(
        public dialogRef: MatDialogRef<StringInputDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: string
    ) { }

    ngOnInit() {
        this.name = new FormControl('');
    }

    close(newGroupName?: string) {
        this.dialogRef.close(newGroupName);
    }

    create() {
        this.close(this.name.value);
    }

}
